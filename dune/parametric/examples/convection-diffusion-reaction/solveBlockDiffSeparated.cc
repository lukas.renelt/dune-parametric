// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

// always include the config file
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/pdelab.hh>

#include "parametricConvDiffProblem.hh"
#include "parametricDiffusionLOP.hh"
#include "parametricSolver.hh"


int main(int argc, char** argv) {
  try {
    Dune::MPIHelper::instance(argc, argv);

    const int dim = 2;
    using RF = double;
    static const std::size_t order = 2;

    // Read parameters from ini file
    Dune::ParameterTree pTree;
    Dune::ParameterTreeParser ptreeparser;
    ptreeparser.readINITree("blockDiffParameters.ini",pTree);
    ptreeparser.readOptions(argc,argv,pTree);

    // make grid
    const int refinement = pTree.get<int>("grid.refinement");

    typedef Dune::YaspGrid<dim> Grid;
    Dune::FieldVector<double, dim> domain(1.0);
    std::array<int, dim> domainDims;

    domain[0] = 1.0;
    domain[1] = 1.0;
    domainDims[0] = pTree.get<int>("grid.yasp_x");
    domainDims[1] = pTree.get<int>("grid.yasp_y");

    auto gridp = std::make_shared<Grid>(domain, domainDims);
    gridp->globalRefine(refinement);
    auto gv = gridp->leafGridView();

    using GV = typename Grid::LeafGridView;

    using Problem = ParametricConvDiffProblem<GV, RF>;
    Problem problem(pTree);
    using Parametrization = BlockDiffusionParametrization;
    Parametrization parametrization;

    // TODO: find a nicer example
    double mu0 = 0.1;
    double mu1 = pTree.get<double>("problem.non-parametric.mu1");
    double mu2 = pTree.get<double>("problem.non-parametric.mu2");
    double mu3 = pTree.get<double>("problem.non-parametric.mu3");

    using Solver = ParametrizedSolver<order,GV,decltype(problem),decltype(parametrization)>;
    Solver solver(gv, problem, parametrization, pTree);
    Dune::ParameterTree solverpTree;
    ptreeparser.readINITree("solver_config.ini", solverpTree);

    int counter = 0;
    while (true) {
      std::cout << "Please pass a new parameter value (bottom left): ";
      std::cin >> mu0;
      if (mu0 < 0)
        return 0;
      std::cout << "Please pass a new parameter value (bottom right): ";
      std::cin >> mu1;
      if (mu1 < 0)
        return 0;
      std::cout << "Please pass a new parameter value (top left): ";
      std::cin >> mu2;
      if (mu2 < 0)
        return 0;
      std::cout << "Please pass a new parameter value (top right): ";
      std::cin >> mu3;
      if (mu3 < 0)
        return 0;
      auto sol = solver.solve({mu0, mu1, mu2, mu3}, solverpTree);
      solver.writeVTK("separated_sol_" + std::to_string(counter));
      counter++;
    }
  }
  catch (Dune::Exception &e) {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (...) {
    std::cerr << "Unknown exception thrown!" << std::endl;

    return 1;
  }
}
