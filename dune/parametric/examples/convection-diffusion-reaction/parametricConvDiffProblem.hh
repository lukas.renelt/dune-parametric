// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_ULTRAWEAK_PARAMETRIC_CONV_DIFF_PROBLEM_HH
#define DUNE_ULTRAWEAK_PARAMETRIC_CONV_DIFF_PROBLEM_HH

#include <cmath>

#include <dune/pdelab.hh>

#include "dune/parametric/common/parametricCoefficientsWrapper.hh"

template <typename GV, typename RF_>
class ParametricConvDiffProblem : public Dune::PDELab::ConvectionDiffusionModelProblem<GV, RF_>
{
public:
  using RF = RF_;
  using Base = Dune::PDELab::ConvectionDiffusionModelProblem<GV,RF>;
  using Traits = typename Base::Traits;

  ParametricConvDiffProblem(Dune::ParameterTree pTree) :
    Base(), pTree_(pTree) {}

  using TensorType = typename Traits::PermTensorType;
  const std::size_t nBlocksX = 2;
  const std::size_t nBlocksY = 2;

  template<typename Element, typename X>
  auto A (const Element& el, const X& x) const
  {
    TensorType identity(0.0);
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      identity[i][i] = 1.0;

    const auto global = el.geometry().global(x);
    std::vector<TensorType> tensors;
    for (std::size_t l=0; l<nBlocksY; l++)
      for (std::size_t k=0; k<nBlocksX; k++)
        tensors.push_back(((global[0]<(k+1) * 1./nBlocksX)
                        and (global[0]>= k * 1./nBlocksX)
                        and (global[1]<(l+1) * 1./nBlocksY)
                        and (global[1]>=l * 1./nBlocksY)) * identity);
    return tensors;
  }

  // Boundary condition type
  template<typename Element, typename X>
  auto bctype(const Element& el, const X& x) const
  {
    //return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
    const auto& global = el.geometry().global(x);
    if (global[1] > 1-1e-10)
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
    else
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
  }

  template<typename Element, typename X>
  auto b (const Element& el, const X& x) const
  {
    return typename Traits::RangeType({0.0, 0.0});
  }

  // reaction coefficient
  template<typename Element, typename X>
  auto c (const Element& el, const X& x) const
  {
    return 0.0;
  }

  // rhs
  template<typename Element, typename X>
  auto f (const Element& el, const X& x) const
  {
    //const auto& global = el.geometry().global(x);
    return 0.0;
  }

  // Dirichlet condition
  template<typename Element, typename X>
  RF g (const Element& el, const X& x) const
  {
    return 0.0;
    const auto& global = el.geometry().global(x);
    return sin(M_PI*global[0]) + sin(M_PI*global[1]);
  }

  // Neumann condition
  template<typename Element, typename X>
  RF j (const Element& el, const X& x) const
  {
    const auto& global = el.geometry().global(x);
    if (global[1] < 1e-10)
      return -1.0;
    return 0.0;
  }

private:
  Dune::ParameterTree pTree_;
};

// TODO: this should adhere to some interface
class BlockDiffusionParametrization {
public:
  static constexpr std::size_t nParams = 4;
  using ParameterValueType = double;
  using ParameterType = std::array<ParameterValueType, nParams>;

  static constexpr std::size_t Qa = 5;
  using ParametrizationBilinear = ParametricCoefficientsWrapper<ParameterType, Qa>;

  static constexpr std::size_t Qf = 1;
  using ParametrizationRhs = ParametricCoefficientsWrapper<ParameterType, Qf>;

  using ParameterFunctionalType =
    typename ParametrizationBilinear::ParameterFunctionalType;

  BlockDiffusionParametrization() {
    std::array<ParameterFunctionalType,Qa> thetas = {
      [](const ParameterType& mu){ return 1.0; },
      [](const ParameterType& mu){ return mu[0]; },
      [](const ParameterType& mu){ return mu[1]; },
      [](const ParameterType& mu){ return mu[2]; },
      [](const ParameterType& mu){ return mu[3]; }
    };

    parametrizationBilinear_ = std::make_shared<ParametrizationBilinear>(thetas);

    // currently no parametrization
    // TODO: make this a default?
    std::array<ParameterFunctionalType,Qf> thetasRhs = {
      [](const ParameterType& mu){ return 1.0; }
    };

    parametrizationRhs_ = std::make_shared<ParametrizationRhs>(std::move(thetasRhs));
  }

  BlockDiffusionParametrization(BlockDiffusionParametrization& other) = delete;
  BlockDiffusionParametrization(const BlockDiffusionParametrization& other) = delete;

  const ParametrizationBilinear& bilinear() const {
    return *parametrizationBilinear_;
  }

  std::shared_ptr<ParametrizationBilinear> bilinearPtr() const {
    return parametrizationBilinear_;
  }

  const ParametrizationRhs& rhs() const {
    return *parametrizationRhs_;
  }

  std::shared_ptr<ParametrizationRhs> rhsPtr() const {
    return parametrizationRhs_;
  }

  // convenience function
  void setParameter(const ParameterType& mu) const {
    parametrizationBilinear_->setParameter(mu);
    parametrizationRhs_->setParameter(mu);
  }

  void initializeFromConfig(Dune::ParameterTree pTree) const {
    const double mu0 = pTree.template get<double>("problem.non-parametric.mu0");
    const double mu1 = pTree.template get<double>("problem.non-parametric.mu1");
    const double mu2 = pTree.template get<double>("problem.non-parametric.mu2");
    const double mu3 = pTree.template get<double>("problem.non-parametric.mu3");

    this->setParameter({mu0, mu1, mu2, mu3});
  }

private:
  std::shared_ptr<ParametrizationBilinear> parametrizationBilinear_;
  std::shared_ptr<ParametrizationRhs> parametrizationRhs_;
};


#endif // DUNE_ULTRAWEAK_PARAMETRIC_CONV_DIFF_PROBLEM_HH
