// -*- tab-width: 2; indent-tabs-mode: nil -*-
#ifndef DUNE_ULTRAWEAK_PARAMETRICCONVDIFFOP_HH
#define DUNE_ULTRAWEAK_PARAMETRICCONVDIFFOP_HH

#include<dune/pdelab.hh>

/** a local operator for solving the linear convection-diffusion equation with standard FEM
*
* \f{align*}{
*   \nabla\cdot(-A(x) \nabla u + b(x) u) + c(x)u &=& f \mbox{ in } \Omega,  \\
*                                              u &=& g \mbox{ on } \partial\Omega_D \\
*                (b(x) u - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N \\
*                        -(A(x)\nabla u) \cdot n &=& o \mbox{ on } \partial\Omega_O
* \f}
* Note:
*  - This formulation is valid for velocity fields which are non-divergence free.
*  - Outflow boundary conditions should only be set on the outflow boundary
*
* \tparam T model of ConvectionDiffusionParameterInterface
*/

template<typename Problem, typename Parametrization, typename FEM>
class ParametrizedConvectionDiffusionOperator :
public Dune::PDELab::NumericalJacobianApplyVolume<
ParametrizedConvectionDiffusionOperator<Problem,Parametrization,FEM> >,
public Dune::PDELab::NumericalJacobianApplyBoundary<
ParametrizedConvectionDiffusionOperator<Problem,Parametrization,FEM> >,
public Dune::PDELab::NumericalJacobianVolume<
ParametrizedConvectionDiffusionOperator<Problem,Parametrization,FEM> >,
public Dune::PDELab::NumericalJacobianBoundary<
ParametrizedConvectionDiffusionOperator<Problem,Parametrization,FEM> >,
public Dune::PDELab::FullVolumePattern,
public Dune::PDELab::LocalOperatorDefaultFlags
{
public:
  enum { doPatternVolume = true };
  enum { doAlphaVolume = true };
  enum { doAlphaBoundary = true };
  enum { doLambdaVolume = true };

  ParametrizedConvectionDiffusionOperator (const Problem& problem_,
                                           const Parametrization& parametrization_,
                                           int intorderadd_=0)
    : problem(problem_), parametrization(parametrization_),
      intorderadd(intorderadd_) {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    // Define types
    using RF = typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType;
    using size_type = typename LFSU::Traits::SizeType;

    // dimensions
    const int dim = EG::Entity::dimension;

    // Reference to cell
    const auto& cell = eg.entity();

    // Get geometry
    auto geo = eg.geometry();

    // evaluate diffusion tensor at cell center, assume it is constant over elements
    auto ref_el = referenceElement(geo);
    auto localcenter = ref_el.position(0,0);
    auto tensors = problem.A(cell,localcenter);

    // Initialize vectors outside for loop
    std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
    Dune::FieldVector<RF,dim> gradu(0.0);
    Dune::FieldVector<RF,dim> Agradu(0.0);
    std::vector<Dune::FieldVector<RF,dim>> Agradus;

    // Transformation matrix
    typename EG::Geometry::JacobianInverseTransposed jac;

    // loop over quadrature points
    auto intorder = intorderadd+2*lfsu.finiteElement().localBasis().order();
    for (const auto& ip : quadratureRule(geo,intorder)) {
      // evaluate basis functions
      auto& phi = cache.evaluateFunction(ip.position(),lfsu.finiteElement().localBasis());

      // evaluate u
      RF u=0.0;
      for (size_type i=0; i<lfsu.size(); i++)
        u += x(lfsu,i)*phi[i];

      // evaluate gradient of shape functions (we assume Galerkin method lfsu=lfsv)
      auto& js = cache.evaluateJacobian(ip.position(),lfsu.finiteElement().localBasis());

      // transform gradients of shape functions to real element
      jac = geo.jacobianInverseTransposed(ip.position());
      for (size_type i=0; i<lfsu.size(); i++)
        jac.mv(js[i][0],gradphi[i]);

      // compute gradient of u
      gradu = 0.0;
      for (size_type i=0; i<lfsu.size(); i++)
        gradu.axpy(x(lfsu,i),gradphi[i]);

      // compute A * gradient of u
      Agradus.clear();
      for (const auto& A : tensors) {
        A.mv(gradu,Agradu);
        Agradus.push_back(Agradu);
      }

      // evaluate velocity field, sink term and source term
      auto b = problem.b(cell,ip.position());
      auto c = problem.c(cell,ip.position());

      // integrate (A grad u)*grad phi_i - u b*grad phi_i + c*u*phi_i
      RF factor = ip.weight() * geo.integrationElement(ip.position());
      for (size_type i=0; i<lfsu.size(); i++) {
        const auto lincomb =
          parametrization.bilinear().template makeParametricLincomb(
  std::array<RF,5>({-u*(b*gradphi[i]) + c*u*phi[i],
             Agradus[0]*gradphi[i],
             Agradus[1]*gradphi[i],
             Agradus[2]*gradphi[i],
             Agradus[3]*gradphi[i]}));
        r.accumulate(lfsu,i, lincomb*factor);
      }
    }
  }

/**
// jacobian of volume term
template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                    M& mat) const
{
// Define types
using RF = typename LFSU::Traits::FiniteElementType::
  Traits::LocalBasisType::Traits::RangeFieldType;
using size_type = typename LFSU::Traits::SizeType;

// dimensions
const int dim = EG::Entity::dimension;

// Reference to cell
const auto& cell = eg.entity();

// Get geometry
auto geo = eg.geometry();

// evaluate diffusion tensor at cell center, assume it is constant over elements
auto ref_el = referenceElement(geo);
auto localcenter = ref_el.position(0,0);
auto tensor = problem.A(cell,localcenter);

// Initialize vectors outside for loop
std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
std::vector<Dune::FieldVector<RF,dim> > Agradphi(lfsu.size());

// Transformation matrix
typename EG::Geometry::JacobianInverseTransposed jac;

// loop over quadrature points
auto intorder = intorderadd+2*lfsu.finiteElement().localBasis().order();
for (const auto& ip : quadratureRule(geo,intorder))
  {
    // update all variables dependent on A if A is not cell-wise constant
    if (!Impl::permeabilityIsConstantPerCell<Problem>(problem))
    {
      tensor = problem.A(cell, ip.position());
    }

    // evaluate gradient of shape functions (we assume Galerkin method lfsu=lfsv)
    auto& js = cache.evaluateJacobian(ip.position(),lfsu.finiteElement().localBasis());

    // transform gradient to real element
    jac = geo.jacobianInverseTransposed(ip.position());
    for (size_type i=0; i<lfsu.size(); i++)
      {
        jac.mv(js[i][0],gradphi[i]);
        tensor.mv(gradphi[i],Agradphi[i]);
      }

    // evaluate basis functions
    auto& phi = cache.evaluateFunction(ip.position(),lfsu.finiteElement().localBasis());

    // evaluate velocity field, sink term and source term
    auto b = problem.b(cell,ip.position());
    auto c = problem.c(cell,ip.position());

    // integrate (A grad phi_j)*grad phi_i - phi_j b*grad phi_i + c*phi_j*phi_i
    RF factor = ip.weight() * geo.integrationElement(ip.position());
    for (size_type j=0; j<lfsu.size(); j++)
      for (size_type i=0; i<lfsu.size(); i++)
        mat.accumulate(lfsu,i,lfsu,j,( Agradphi[j]*gradphi[i]-phi[j]*(b*gradphi[i])+c*phi[j]*phi[i] )*factor);
  }
}
*/

  // boundary integral
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_boundary (const IG& ig,
                     const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                     R& r_s) const
  {
  // Define types
  using RF = typename LFSV::Traits::FiniteElementType::
    Traits::LocalBasisType::Traits::RangeFieldType;
  using size_type = typename LFSV::Traits::SizeType;

  // Reference to the inside cell
  const auto& cell_inside = ig.inside();

  // Get geometry
  auto geo = ig.geometry();

  // Get geometry of intersection in local coordinates of cell_inside
  auto geo_in_inside = ig.geometryInInside();

  // evaluate boundary condition type
  auto ref_el = referenceElement(geo_in_inside);
  auto local_face_center = ref_el.position(0,0);
  auto intersection = ig.intersection();
  auto bctype = problem.bctype(intersection,local_face_center);

  // skip rest if we are on Dirichlet boundary
   if (bctype==Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet) return;

  // loop over quadrature points and integrate normal flux
  auto intorder = intorderadd+2*lfsu_s.finiteElement().localBasis().order();
  for (const auto& ip : quadratureRule(geo,intorder))
    {
      // position of quadrature point in local coordinates of element
      auto local = geo_in_inside.global(ip.position());

      // evaluate shape functions (assume Galerkin method)
      auto& phi = cache.evaluateFunction(local,lfsu_s.finiteElement().localBasis());

      if (bctype==Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann)
        {
          // evaluate flux boundary condition
          auto j = problem.j(intersection,ip.position());

          // integrate j
          auto factor = ip.weight()*geo.integrationElement(ip.position());
          for (size_type i=0; i<lfsu_s.size(); i++)
            r_s.accumulate(lfsu_s,i,j*phi[i]*factor);
        }

      if (bctype==Dune::PDELab::ConvectionDiffusionBoundaryConditions::Outflow)
        {
          // evaluate u
          RF u=0.0;
          for (size_type i=0; i<lfsu_s.size(); i++)
            u += x_s(lfsu_s,i)*phi[i];

          // evaluate velocity field and outer unit normal
          auto b = problem.b(cell_inside,local);
          auto n = ig.unitOuterNormal(ip.position());

          // evaluate outflow boundary condition
          auto o = problem.o(intersection,ip.position());

          // integrate o
          auto factor = ip.weight()*geo.integrationElement(ip.position());
          for (size_type i=0; i<lfsu_s.size(); i++)
            r_s.accumulate(lfsu_s,i,( (b*n)*u + o)*phi[i]*factor);
        }
    }
  }

  // jacobian contribution from boundary
  template<typename IG, typename LFSU, typename X, typename LFSV, typename M>
  void jacobian_boundary (const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          M& mat_s) const {
    // Define types
    using size_type = typename LFSV::Traits::SizeType;

    // Reference to the inside cell
    const auto& cell_inside = ig.inside();

    // Get geometry
    auto geo = ig.geometry();

    // Get geometry of intersection in local coordinates of cell_inside
    auto geo_in_inside = ig.geometryInInside();

    // evaluate boundary condition type
    auto ref_el = referenceElement(geo_in_inside);
    auto local_face_center = ref_el.position(0,0);
    auto intersection = ig.intersection();
    auto bctype = problem.bctype(intersection,local_face_center);

    // skip rest if we are on Dirichlet or Neumann boundary
    if (bctype==Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet) return;
    if (bctype==Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann) return;

    // loop over quadrature points and integrate normal flux
    auto intorder = intorderadd+2*lfsu_s.finiteElement().localBasis().order();
    for (const auto& ip : quadratureRule(geo,intorder)) {
      // position of quadrature point in local coordinates of element
      auto local = geo_in_inside.global(ip.position());

      // evaluate shape functions (assume Galerkin method)
      auto& phi = cache.evaluateFunction(local,lfsu_s.finiteElement().localBasis());

      // evaluate velocity field and outer unit normal
      auto b = problem.b(cell_inside,local);
      auto n = ig.unitOuterNormal(ip.position());

      // integrate
      auto factor = ip.weight()*geo.integrationElement(ip.position());
      for (size_type j=0; j<lfsu_s.size(); j++)
        for (size_type i=0; i<lfsu_s.size(); i++)
          mat_s.accumulate(lfsu_s,i,lfsu_s,j,(b*n)*phi[j]*phi[i]*factor);
      }
  }

  template<typename EG, typename LFSV, typename R>
  void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const {
    using RF = typename
      LFSV::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType;
    using size_type = typename LFSV::Traits::SizeType;

    const auto& cell = eg.entity();
    const auto geo = eg.geometry();

    auto intorder = intorderadd+2*lfsv.finiteElement().localBasis().order();
    for (const auto& qp : quadratureRule(geo, intorder)) {
      const auto fval = problem.f(cell, qp.position());

      auto& psi = cache.evaluateFunction(qp.position(), lfsv.finiteElement().localBasis());

      RF integrationFactor = qp.weight() * geo.integrationElement(qp.position());
      for (size_type i=0; i<lfsv.size(); i++)
        r.accumulate(lfsv, i, integrationFactor * -fval * psi[i]);
    }
  }

private:
  const Problem& problem;
  const Parametrization& parametrization;
  int intorderadd;
  using LocalBasisType = typename FEM::Traits::FiniteElementType::Traits::LocalBasisType;
  Dune::PDELab::LocalBasisCache<LocalBasisType> cache;
};

#endif // DUNE_ULTRAWEAK_PARAMETRICCONVDIFFOP_HH
