// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

// always include the config file
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/pdelab.hh>

#include "parametricConvDiffProblem.hh"
#include "parametricDiffusionLOP.hh"


int main(int argc, char** argv) {
  try {
    Dune::MPIHelper::instance(argc, argv);

    const int dim = 2;
    using RF = double;
    static const std::size_t order = 2;

    // Read parameters from ini file
    Dune::ParameterTree pTree;
    Dune::ParameterTreeParser ptreeparser;
    ptreeparser.readINITree("blockDiffParameters.ini",pTree);
    ptreeparser.readOptions(argc,argv,pTree);

    // make grid
    const int refinement = pTree.get<int>("grid.refinement");

    typedef Dune::YaspGrid<dim> Grid;
    Dune::FieldVector<double, dim> domain(1.0);
    std::array<int, dim> domainDims;

    domain[0] = 1.0;
    domain[1] = 1.0;
    domainDims[0] = pTree.get<int>("grid.yasp_x");
    domainDims[1] = pTree.get<int>("grid.yasp_y");

    auto gridp = std::make_shared<Grid>(domain, domainDims);
    gridp->globalRefine(refinement);
    auto gv = gridp->leafGridView();

    using GV = typename Grid::LeafGridView;

    using Problem = ParametricConvDiffProblem<GV, RF>;
    Problem problem(pTree);
    using Parametrization = BlockDiffusionParametrization;
    Parametrization parametrization;

    // classic PDELab code
    using DF = typename GV::Grid::ctype;
    using RF = double;
    using FEM = Dune::PDELab::QkLocalFiniteElementMap<GV,DF,RF,order>;
    FEM fem(gv);

    using CON = Dune::PDELab::ConformingDirichletConstraints;
    using VBE = Dune::PDELab::ISTL::VectorBackend<>;
    using GFS = Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE>;
    GFS gfs(gv,fem);
    gfs.name("Q2");

    using Coeff = Dune::PDELab::Backend::Vector<GFS, RF>;
    Coeff sol(gfs);

    using LOP = ParametrizedConvectionDiffusionOperator<Problem,Parametrization,FEM>;
    LOP lop(problem, parametrization);

    using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
    MBE mbe(1<<(dim+1));

    using CC = typename GFS::template ConstraintsContainer<RF>::Type;
    CC cc;
    cc.clear();
    Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> bctype(gv,problem);
    Dune::PDELab::constraints(bctype,gfs,cc);

    using GO = Dune::PDELab::GridOperator<GFS, GFS, LOP, MBE, RF, RF, RF, CC, CC>;
    GO go(gfs, cc, gfs, cc, lop, mbe);

    parametrization.initializeFromConfig(pTree);

    const int verbosity = 1;
    using LS = Dune::PDELab::ISTLBackend_SEQ_SuperLU;
    LS ls(verbosity);

    using Solver = Dune::PDELab::StationaryLinearProblemSolver<GO,LS,Coeff>;
    Solver solver(go, ls, sol, pTree.get<double>("reduction"));
    solver.apply();

    using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
    Dune::RefinementIntervals sub(2);
    VTKWriter vtkwriter(gv, sub);

    std::string filename("blockDiffSol");
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter, gfs, sol,
                                         Dune::PDELab::vtk::defaultNameScheme());
    vtkwriter.write(filename, Dune::VTK::ascii);
  }
  catch (Dune::Exception &e) {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (...) {
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}
